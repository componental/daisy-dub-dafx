# Daisy Dub DAFx workshop
<img src="https://upload.wikimedia.org/wikipedia/commons/7/70/CC_BY-NC-ND.svg"  width="60"></img>
[![Issues](https://img.shields.io/gitlab/issues/open-raw/41280786?color=red)](https://gitlab.com/componental/daisy-dub-dafx/-/issues)
[![Issues](https://img.shields.io/gitlab/issues/closed-raw/41280786?color=green)](https://gitlab.com/componental/daisy-dub-dafx/-/issues)
[![MergeRequests](https://img.shields.io/gitlab/merge-requests/open/41280786)](https://gitlab.com/componental/daisy-dub-dafx/-/merge_requests)
![Version](https://img.shields.io/badge/Version-1.5.0-yellow.svg)
![Hardware Status](https://img.shields.io/badge/Hardware-Prototype-yellow.svg)


[comment]: [![IMAGE_ALT](https://img.youtube.com/vi/fcGmwTvWMJk/hqdefault.jpg)](https://youtu.be/fcGmwTvWMJk)


<img src="https://i.imgur.com/uXooAq9.jpeg" alt="Daisy Dub alpha V0 and beta V1 prototype" width="80%"/>




***

## Welcome to the workshop
We are thrilled to be hosting this hackathon workshop during DAFx 2023 at Aalborg University in Copenhagen.

## Installation
Always check out the latest installation instructions on [Electrosmith github](https://github.com/electro-smith/DaisyWiki/wiki/1.-Setting-Up-Your-Development-Environment).

**macOS M1 / M2 processor**
- Install M1/M2 version of tool chain via [Electrosmith installer](https://media-obsy-dev.fra1.cdn.digitaloceanspaces.com/installers/DaisyToolchain-macos-installer-x64-0.1.3-ARM.pkg).
- Install this version specifically: **[arm-none-eabi-gcc v10.3-2021.10](https://developer.arm.com/downloads/-/gnu-rm)** - check in terminal with `arm-none-eabi-gcc --version`

### Oopsy Max Package
## **ONLY USE COMPONENTAL'S MODIFIED VERSION OF OOPSY OR IT WILL FRY THE OLED ON V1.5!**
- Download Componental's modified version of Oopsy Max Package with the **bootloader-additions** branch via git: `git clone https://github.com/rallevondalle/oopsy.git` and run ./install.sh
- Change branch to **bootloader-additions** if you downloaded via git ([otherwise, get the zip version here](https://github.com/rallevondalle/oopsy/archive/refs/heads/bootloader-additions.zip))
- MAKE SURE YOU CHANGE BRANCH
- Add path to Oopsy Max package to Max path.

When flasing, **always use the JSON file provided by Componental from [this folder](https://gitlab.com/componental/daisy-dub-dafx/-/tree/main/json?ref_type=heads)**


## Description
Daisy Dub is a versatile and modular quadraphonic real-time audio effect unit. The device utilises the Daisy Seed microprocessor by Electrosmith and offers high flexibility and customisation for music production and live performance. It features a range of real-time audio effects, with an emphasis on creative delays and includes a range of modulation effects and filters. In addition, the unit is compact and portable, with an interactive graphical interface, four knobs and two arcade buttons for performance control, an encoder for menu diving, and an OLED screen settings and spectrum analysis. Daisy Dub is built with Gen by Cycling ’74 and C++ and took inspiration from various hardware audio delay units.

The Daisy Dub offers a modern take on contemporary real-time audio effects, emphasising a delay effect, quadrophonic audio processing, and an interactive graphical interface that allows for complex multi-channel audio manipulation. In addition, its compact and portable design, USB Type-C power delivery, and compatibility with various audio equipment make it a convenient and practical tool for audio professionals.

## Demo video of alpha prototype
[![IMAGE_ALT](https://img.youtube.com/vi/fcGmwTvWMJk/hqdefault.jpg)](https://youtu.be/fcGmwTvWMJk)

## Usage and examples
Check [audio examples folder](https://gitlab.com/northernstructuresaudio/daisy-dub/-/tree/main/audio-examples).

## Support
Get in touch via [LinkedIn](https://www.linkedin.com/company/northern-structures-aps/).

## Roadmap
Spring 2023: Researching sustainable case design and production. Researching manufacturing in California and Europe.
Summer 2023: Usability testing of prototype hardware version 1.5 as workshops where participants assemble the unit.


## Contributing
We are open to contributions.

## Authors and acknowledgment
Created by **[Rasmus Kjærbo](https://www.linkedin.com/in/rasmuskjaerbo/), [Leo Fogadić](https://www.linkedin.com/in/leof0/), and [Oliver Bjørk Winkel](https://www.linkedin.com/in/oliver-bj%C3%B8rk-winkel-370682a2/)** at [Sound and Music Computing](https://www.en.aau.dk/education/master/sound-and-music-computing), Aalborg University, Copenhagen and [Rumkraft](https://rumkraft.dk). Huge thanks to [SoundHub Denmark](https://soundhub.dk/), especially Jens and Dirk. Also a massive biggup and hug to the wizard Andreas Wetterberg and Stephen Hensley from Electrosmith. And of course to Stefania Serafin from Aalborg University, Copenhagen for continuous support, guidance and karaoke.

## License
See [License](https://gitlab.com/smcmasterthesis/daisy-dub-delay/-/blob/main/LICENSE).

## Project status
- Quadraphonic audio implemented!
- Final evaluation of v1.0 hardware design
- Researching sustainable case design and production
- Researching manufacturing
- Optimise DSP code
- Developing new algorithms (hackathon!)
